package pattarapol;

public class Trainer {
    private String name;
    private Pokemon pk;
    private int score;
    private int choose;


    public Trainer(String name, Pokemon pk, int score,int choose) {
        this.name = name;
        this.pk = pk;
        this.score = score;
		this.choose = choose;

    }

    public String toString() {
        return name + " " + pk + " "+"score: "+score;
    }

    public Pokemon getpk() {
        return pk;
    }

    public String getname() {
        return name;
    }
    public int getscore(int win) {
		return score = win;
	}
	public int getscore() {
		return score;
	}
    public int getchoose() {
		return choose;
	}
}
