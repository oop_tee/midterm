package pattarapol;

import java.util.Scanner;

public class PokemonGame {
	static void prinWelcome() {
		System.out.println("Welcome to Pokemon Game!!!");
	}

	static void printStart() {
		System.out.println("--Menu--");
		System.out.println("1. Start Game");
		System.out.println("2. Exit Game");
	}

	static int inputChoice() {
		int choice;
		Scanner kb = new Scanner(System.in);
		while (true) {
			System.out.print("Please input your choice(1-2): ");
			choice = kb.nextInt();
			if (choice >= 1 && choice <= 2) {
				return choice;
			}
			System.out.println("Error: Please input between 1-2");
		}

	}

	public static void main(String[] args) {
		int choice = 0;
		while (true) {
			prinWelcome();
			printStart();
			choice = inputChoice();
			process(choice);
		}
	}

	static void process(int choice) {
		switch (choice) {
			case 1:
				GamePokemon();

				break;
			case 2:
				exitProgram();
				break;
		}
	}

	static void GamePokemon() {

		Scanner kb = new Scanner(System.in);
		Pokemon[] pk = new Pokemon[8];
		Trainer[] tn = new Trainer[5];
		int win = 0;
		int max = Integer.MIN_VALUE;
		int check = 0;
		boolean found;

		pk[0] = new Pokemon(1, "Charmander", "fire", 0);
		pk[1] = new Pokemon(2, "Squirle", "water", 0);
		pk[2] = new Pokemon(3, "Balbasaur", "grass", 0);
		pk[3] = new Pokemon(4, "Greninja", "water", 0);
		pk[4] = new Pokemon(5, "Treecko", "grass", 0);
		pk[5] = new Pokemon(6, "Suicune", "water", 0);
		pk[6] = new Pokemon(7, "Flareon", "fire", 0);
		pk[7] = new Pokemon(8, "Ninetales", "fire", 0);

		System.out.println("-------------------------------------------");
		for (int i = 0; i < pk.length; i++) {
			pk[i].random();
			System.out.println(" " + pk[i]);
			System.out.println("-------------------------------------------");
		}

		System.out.print("Enter name :");
		String name = kb.next();
		System.out.print(name + " : choose your Pokemon Number: ");
		int choose = kb.nextInt();
		while (choose > 8 || choose < 1) {
			System.out.print("please select number 1 - 8: ");
			choose = kb.nextInt();
		}
		tn[check] = new Trainer(name, pk[choose - 1], 0,choose);
		check++;

		found = false;

		while (check != 5) {
			if (found == true) {
				break;
			} else if (found == false) {
				System.out.print("Enter name :");
				name = kb.next();
				System.out.print(name + " : choose your Pokemon Number: ");
				choose = kb.nextInt();
				while (choose > 8 || choose < 1) {
					System.out.print("please select number 1 - 8: ");
					choose = kb.nextInt();
				}
				for (int i = 0; i < check; i++) {
					if (choose == tn[i].getchoose()) {
						found = true;
						break;
					}
				
				}
				if (found == false) {
					tn[check] = new Trainer(name, pk[choose - 1], 0,choose);
				}	
			}
		
			check++;

		}
		if (found == false) {
			for (int i = 0; i < tn.length; i++) {
				for (int k = 0; k < tn.length; k++) {
					if (tn[i].getpk().getelement() == "fire") {
						if (tn[k].getpk().getelement() == "grass") {
							if (tn[i].getpk().getnewpower() > tn[k].getpk().getpower()) {
								win++;
								tn[i].getscore(win);
							} else if (tn[i].getpk().getnewpower() < tn[k].getpk().getpower()) {
								win--;
								tn[i].getscore(win);
							}
						} else if (tn[k].getpk().getelement() == "water") {
							if (tn[i].getpk().getpower() > tn[k].getpk().getnewpower()) {
								win++;
								tn[i].getscore(win);
							} else if (tn[i].getpk().getpower() < tn[k].getpk().getnewpower()) {
								win--;
								tn[i].getscore(win);
							}
						} else {
							if (tn[i].getpk().getpower() > tn[k].getpk().getpower()) {
								win++;
								tn[i].getscore(win);
							} else if (tn[i].getpk().getpower() < tn[k].getpk().getpower()) {
								win--;
								tn[i].getscore(win);
							}
						}

					}
					if (tn[i].getpk().getelement() == "water") {
						if (tn[k].getpk().getelement() == "fire") {
							if (tn[i].getpk().getnewpower() > tn[k].getpk().getpower()) {
								win++;
								tn[i].getscore(win);
							} else if (tn[i].getpk().getnewpower() < tn[k].getpk().getpower()) {
								win--;
								tn[i].getscore(win);
							} else if (tn[k].getpk().getelement() == "grass") {
								if (tn[i].getpk().getpower() > tn[k].getpk().getnewpower()) {
									win++;
									tn[i].getscore(win);
								} else if (tn[i].getpk().getpower() < tn[k].getpk().getnewpower()) {
									win--;
									tn[i].getscore(win);
								}
							} else {
								if (tn[i].getpk().getpower() > tn[k].getpk().getpower()) {
									win++;
									tn[i].getscore(win);
								} else if (tn[i].getpk().getpower() < tn[k].getpk().getpower()) {
									win--;
									tn[i].getscore(win);
								}
							}

						}
					}
					if (tn[i].getpk().getelement() == "grass") {
						if (tn[k].getpk().getelement() == "water") {
							if (tn[i].getpk().getnewpower() > tn[k].getpk().getpower()) {
								win++;
								tn[i].getscore(win);
							} else if (tn[i].getpk().getnewpower() < tn[k].getpk().getpower()) {
								win--;
								tn[i].getscore(win);
							}
						} else if (tn[k].getpk().getelement() == "fire") {
							if (tn[i].getpk().getpower() > tn[k].getpk().getnewpower()) {
								win++;
								tn[i].getscore(win);
							} else if (tn[i].getpk().getpower() < tn[k].getpk().getnewpower()) {
								win--;
								tn[i].getscore(win);
							}
						} else {
							if (tn[i].getpk().getpower() > tn[k].getpk().getpower()) {
								win++;
								tn[i].getscore(win);
							} else if (tn[i].getpk().getpower() < tn[k].getpk().getpower()) {
								win--;
								tn[i].getscore(win);
							}

						}
					}
				}
				win = 0;
			}
			System.out.println();
			System.out.println("-------------------------------------------");
			System.out.println("               SCORE BOARD                 ");
			System.out.println("-------------------------------------------");
			for (int i = 0; i < tn.length; i++) {
				if (tn[i].getscore() > max) {
					max = tn[i].getscore();
				}
				System.out.println(tn[i]);
				System.out.println("-------------------------------------------");
			}
			for (int i = 0; i < tn.length; i++) {
				if (tn[i].getscore() == max) {
					System.out.println();
					System.out.println("!!!!!!THE WINER!!!!!! :" + tn[i].getname());
				}
			}
		} else {
			System.out.println("Error!!! Plase restart the game");
			System.out.println("Error!!! Plase restart the game");
			System.out.println("Error!!! Plase restart the game");
		}
		System.out.println();
	}

	static void exitProgram() {
		System.out.println("Bye!!!");
		System.exit(0);
	}

}
