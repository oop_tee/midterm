package pattarapol;

public class Pokemon {
    private String name;
	private String element;
	private int power1;
	private int power2;
    private int number;

	public Pokemon(int number,String name, String element, int power) {
        this.number = number;
		this.name = name;
		this.element = element;
		this.power1 = power;

	}

	public int random() {
		return power1 = (int) (Math.random() * (40 - 10 + 1) + 10);
	}
	public String toString() {
		return "Number:"+number + " Pokemon name: " + name + " element: " + element + " power: " + power1;
	}
	public int getpower() {
		return power1;
	}
	public int getnewpower() {
		return power2 = power1*2;
	}
	
	public String getelement() {
		return element;
	}
    
}

	